import React from 'react';
import ReactDOM from 'react-dom';
import {Flex, FlexItem} from 'cf-component-flex';
import Text from 'cf-component-text';
import Codemirror from 'react-codemirror';
import Remarkable from 'remarkable';

var md = new Remarkable();

class LeftBox extends React.Component {

    constructor(props) {
        super(props);
        this.changeText = this.changeText.bind(this);
    }

    changeText(e) {
        this.props.updateState(e.target.value);
    }

    render() {
        return (<textarea rows="20" cols="100" value={this.props.text} onChange={this.changeText}></textarea>);     
    }
}

class RightBox extends React.Component {
    render() {
        var text = md.render(this.props.text);
        return <div dangerouslySetInnerHTML={{__html: text}}></div>;        
    }
}

class App extends React.Component {

   constructor(props) {
      super(props);
        
      this.state = {
         text: '# Hellooooooo'
      }

      this.updateState = this.updateState.bind(this);

   };

   updateState(text) {
      this.setState({text: text})
   }

   render() {
      return (
        <div>
            <div>
                <Text type="success" align="center">Hello World</Text>
            </div>
            <Flex spacing="thin">
              <FlexItem><LeftBox text={this.state.text} updateState={this.updateState} /></FlexItem>
              <FlexItem><RightBox text={this.state.text} /></FlexItem>
            </Flex>
        </div>
      );
   }
}

export default App;